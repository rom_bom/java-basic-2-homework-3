package com.company;

public class Main {

    public static void main(String[] args) {

        byte variable1 = 77;
        short variable2 = 3578;
        int variable3 = 135_543_578;
        long variable4 = 1_135_543_578L;
        float variable5 = 5_321.745_543f;
        double variable6 = 375_321.745_333;
        char variable7 = 'V';
        boolean variable8 = true;

        int largeNumber1 = 10_000_000;
        float largeNumber2 = 100.066_123_455f;
        double largeNumber3 = 15_500_088.4_433_112_201;

        char symbol1 = '\u00a9';
        char symbol2 = '\u00b5';
        char symbol3 = '\u00ae';


        System.out.println("Значення типу byte: " + variable1);
        System.out.println("Значення типу short: " + variable2);
        System.out.println("Значення типу int: " + variable3);
        System.out.println("Значення типу long: " + variable4);
        System.out.println("Значення типу float: " + variable5);
        System.out.println("Значення типу double: " + variable6);
        System.out.println("Значення типу char: " + variable7);
        System.out.println("Значення типу true: " + variable8);
        System.out.println();
        System.out.println("Значення з розділювачем типу int: " + largeNumber1);
        System.out.println("Значення з розділювачем типу float: " + largeNumber2);
        System.out.println("Значення з розділювачем типу double: " + largeNumber3);
        System.out.println();
        System.out.println(symbol1 + " " + symbol2 + " " + symbol3);
    }
}
